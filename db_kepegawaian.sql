-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 01, 2019 at 12:33 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kepegawaian`
--

-- --------------------------------------------------------

--
-- Table structure for table `bidang`
--

CREATE TABLE `bidang` (
  `id_bidang` int(11) NOT NULL,
  `nama_bidang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bidang`
--

INSERT INTO `bidang` (`id_bidang`, `nama_bidang`) VALUES
(1, 'Sekretariat'),
(2, 'Pengelolaan Informasi Administrasi Kependudukan'),
(4, 'Pelayanan Pencatatan Sipil'),
(5, 'Pemanfaatan Data dan Inovasi Pelayanan'),
(6, 'Pelayanan Pendaftaran Penduduk');

-- --------------------------------------------------------

--
-- Table structure for table `diklat`
--

CREATE TABLE `diklat` (
  `id_diklat` int(11) NOT NULL,
  `nip` char(18) NOT NULL,
  `nama_diklat` varchar(100) NOT NULL,
  `tipe_diklat` varchar(50) NOT NULL,
  `lama_diklat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diklat`
--

INSERT INTO `diklat` (`id_diklat`, `nip`, `nama_diklat`, `tipe_diklat`, `lama_diklat`) VALUES
(2, '197405131993021001', 'ADUM Tahun 2000', 'Diklat Managerial', 275),
(4, '196401161993032002', 'Diklatpim Tk. III Tahun 2011 ', 'Diklat Managerial', 396),
(5, '197405152003121006', 'Diklatpim Tk. IV Tahun 2007', 'Diklat Managerial', 336),
(6, '196212311981121018', 'Diklatpim Tk. IV Tahun 2007', 'Diklat Managerial', 336),
(7, '197606162001122004', 'Diklatpim Tk. IV Tahun 2007', 'Diklat Managerial', 336),
(8, '197901161998102001', 'Diklatpim Tk. III Tahun 2017 ', 'Diklat Managerial', 0),
(9, '196712311992021003', 'Diklatpim Tk. IV Tahun 2006', 'Diklat Managerial', 336),
(10, '196101161981032008', 'ADUMLA Tahun 2000', 'Diklat Managerial', 360),
(11, '196008011983031020', 'Diklatpim Tk. IV Tahun 2008', 'Diklat Managerial', 336),
(12, '196510131986031008', 'Diklatpim Tk. IV Tahun 2008', 'Diklat Managerial', 336),
(13, '196312011987021002', 'Diklatpim Tk. IV Tahun 2009', 'Diklat Managerial', 321),
(14, '197210202001121003', 'Diklatpim Tk. IV Tahun 2008', 'Diklat Managerial', 336),
(15, '196008231986031015', 'Pendidikan dan Pelatihan Management of Training 2003', 'Bimtek/Workshop', 40),
(16, '197602022006041018', 'Diklatpim Tk. IV Tahun 2011', 'Diklat Managerial', 321);

-- --------------------------------------------------------

--
-- Table structure for table `eselon`
--

CREATE TABLE `eselon` (
  `id_eselon` int(11) NOT NULL,
  `eselon` varchar(5) NOT NULL,
  `id_pangkat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eselon`
--

INSERT INTO `eselon` (`id_eselon`, `eselon`, `id_pangkat`) VALUES
(1, 'I-a', 20),
(2, 'I-a', 21),
(3, 'I-b', 19),
(4, 'I-b', 20),
(5, 'I-b', 21),
(6, 'II-a', 19),
(7, 'II-a', 20),
(8, 'II-b', 18),
(9, 'II-b', 19),
(10, 'III-a', 17),
(11, 'III-a', 18),
(12, 'III-b', 16),
(13, 'III-b', 17),
(14, 'IV-a', 15),
(15, 'IV-a', 16),
(16, 'IV-b', 14),
(17, 'IV-b', 15),
(18, 'V-a', 13),
(34, 'V-a', 14);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `tipe_jabatan` char(1) NOT NULL,
  `nama_jabatan` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `tipe_jabatan`, `nama_jabatan`) VALUES
(9, 'A', 'Kepala Dinas Kependudukan dan Pencatatan Sipil'),
(10, 'A', 'Sekretaris'),
(11, 'A', 'Kepala Bidang'),
(12, 'A', 'Kepala Seksi'),
(13, 'A', 'Kepala Sub Bagian'),
(14, 'C', 'Pengolah Data Laporan PertanggungJawaban Kegiatan'),
(15, 'C', 'Penyusun Rencana Kegiatan dan Anggaran'),
(16, 'C', 'Pengelola Teknologi Informasi'),
(17, 'C', 'Registrar'),
(18, 'C', 'Pengelola Sistem dan Jaringan'),
(19, 'C', 'Pengolah Data'),
(20, 'C', 'Penyusun Kebutuhan Barang Inventaris'),
(21, 'C', 'Penyusun Pencatatan dan Pelaporan Data Kependudukan'),
(22, 'C', 'Pengadministrasi Kependudukan'),
(23, 'C', 'Bendahara'),
(24, 'C', 'Analis Kerjasama'),
(25, 'C', 'Pengelola Database'),
(26, 'C', 'Pengadministrasi Umum'),
(27, 'C', 'Arsiparis'),
(28, 'C', 'Pengelola Gaji'),
(29, 'C', 'Pengolah Data Kebijakan Klasifikasi Barang'),
(30, 'C', 'Pengelola Database Surat Perintah Membayar'),
(31, 'C', 'Pengadministrasi Akta Kelahiran'),
(32, 'C', 'Pengelola Bahan Perencanaan'),
(33, 'C', 'Pengelola Data Administrasi dan Verifikasi'),
(34, 'C', 'Pengelola Kepegawaian'),
(35, 'C', 'Pengelola Sistem Informasi Kependudukan');

-- --------------------------------------------------------

--
-- Table structure for table `pangkat`
--

CREATE TABLE `pangkat` (
  `id_pangkat` int(11) NOT NULL,
  `nama_pangkat` varchar(100) NOT NULL,
  `gol_ruang` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pangkat`
--

INSERT INTO `pangkat` (`id_pangkat`, `nama_pangkat`, `gol_ruang`) VALUES
(4, 'Juru Muda', 'I.a'),
(5, 'Juru Muda Tingkat I', 'I.b'),
(6, 'Juru', 'I.c'),
(7, 'Juru Tingkat I', 'I.d'),
(8, 'Pengatur Muda', 'II.a'),
(9, 'Pengatur Muda Tingkat I', 'II.b'),
(10, 'Pengatur', 'II.c'),
(11, 'Pengatur Tingkat I', 'II.d'),
(13, 'Penata Muda', 'III.a'),
(14, 'Penata Muda Tingkat I', 'III.b'),
(15, 'Penata', 'III.c'),
(16, 'Penata Tingkat I', 'III.d'),
(17, 'Pembina', 'IV.a'),
(18, 'Pembina Tingkat I', 'IV.b'),
(19, 'Pembina Utama Muda', 'IV.c'),
(20, 'Pembina Utama Madya', 'IV.d'),
(21, 'Pembina Utama', 'IV.e');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `nip` char(18) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` char(1) NOT NULL,
  `id_pangkat` int(11) NOT NULL,
  `tamat_pangkat_terakhir` date NOT NULL,
  `mulai_bekerja` date NOT NULL,
  `id_eselon` int(11) DEFAULT NULL,
  `tamat_eselon` date DEFAULT NULL,
  `id_bidang` int(11) NOT NULL,
  `id_sub_bidang` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`nip`, `nama`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `id_pangkat`, `tamat_pangkat_terakhir`, `mulai_bekerja`, `id_eselon`, `tamat_eselon`, `id_bidang`, `id_sub_bidang`, `id_jabatan`, `keterangan`) VALUES
('196008011983031020', 'MUHADJIR, SH', 'Bangkalan', '1960-08-01', 'L', 16, '2013-10-01', '1983-03-01', 15, '2017-01-20', 6, 2, 12, NULL),
('196008231986031015', 'ABDUL HAMED', 'Bangkalan', '1960-08-23', 'L', 16, '2016-04-01', '1986-03-01', 15, '2017-01-20', 2, 7, 12, NULL),
('196101161981032008', 'ADILAT ANNA MUTUANINGSIH, BcKN', 'Blora', '1961-01-16', 'P', 16, '2009-10-01', '1981-03-01', 15, '2017-01-20', 4, 1, 12, NULL),
('196212311981121018', 'SYAMSUL BAKRI, SH., MM', 'Bangkalan', '1962-12-31', 'L', 17, '2015-04-01', '1981-12-01', 13, '2017-01-20', 2, 3, 11, NULL),
('196312011987021002', 'AKHMAD IMBRAN', 'Pamekasan', '1963-12-01', 'L', 16, '2014-04-01', '1987-02-01', 15, '2017-01-20', 6, 4, 12, NULL),
('196401161993032002', 'Ir. R. ELLY FARIDA', 'Bangkalan', '1964-01-16', 'P', 18, '2015-10-10', '1993-03-01', 8, '2017-01-20', 1, 0, 10, NULL),
('196405051988031013', 'PARMADI', 'Bangkalan', '1964-05-05', 'L', 14, '2008-10-01', '1988-03-01', 0, NULL, 4, 1, 14, NULL),
('196406152001121002', 'RADEN ISKRAK GAMALIANTO, S. Kom', 'Bangkalan', '1964-06-15', 'L', 15, '2018-04-01', '2001-12-01', 0, NULL, 4, 5, 17, NULL),
('196509302001122002', 'SEPTY FARIANI, BPA', 'Bangkalan', '1965-09-30', 'P', 14, '2018-04-01', '2001-12-01', 0, NULL, 5, 13, 24, NULL),
('196510131986031008', 'SALAMET RIYADI, S.Sos', 'Pamekasan', '1965-10-13', 'L', 16, '2013-10-01', '1986-03-01', 15, '2017-01-20', 2, 3, 12, NULL),
('196603121987031008', 'MOHAMMAD  MUHID, S. Sos', 'Bangkalan', '1966-03-12', 'L', 15, '2017-10-01', '1987-03-01', 0, NULL, 1, 15, 14, NULL),
('196612311988031044', 'MOH. SYAFI\'E', 'Pamekasan', '1966-12-31', 'L', 14, '2009-04-01', '1988-03-01', 0, NULL, 1, 9, 20, NULL),
('196709192010011001', 'RUDI DERMAWAN, Amd', 'Bangkalan', '1967-09-19', 'L', 11, '2014-04-01', '2010-01-01', 0, NULL, 4, 10, 17, NULL),
('196712311992021003', 'Drs. SUCIPTO', 'Sumenep', '1967-12-31', 'L', 16, '2013-04-01', '1992-02-01', 15, '2017-01-20', 4, 5, 12, NULL),
('196804281993032010', 'ANI IRIL SUMIARSIH. SH', 'Bangkalan', '1968-04-28', 'P', 15, '2017-10-01', '1993-03-01', 0, NULL, 2, 7, 16, NULL),
('196902132007011016', 'MUDARI', 'Bangkalan', '1969-02-13', 'L', 10, '2015-04-01', '2007-01-01', 0, NULL, 4, 10, 27, NULL),
('197201082007011012', 'IMAM JAUHARI', 'Bangkalan', '1972-01-08', 'L', 10, '2018-04-01', '2007-01-01', 0, NULL, 1, 15, 28, NULL),
('197210202001121003', 'RADEN TANJIS HAMDAN, SE', 'Bangkalan', '1972-10-20', 'L', 16, '2013-10-01', '2001-12-01', 15, '2017-01-20', 6, 6, 12, NULL),
('197308182009032001', 'ANIS JAUMILLAH, SE', 'Bangkalan', '1973-08-18', 'P', 13, '2016-10-01', '2009-03-01', 0, '1970-01-01', 6, 2, 17, NULL),
('197405131993021001', 'RUDIYANTO, S.sos., MM', 'Blitar', '1974-05-13', 'L', 19, '2018-04-01', '1993-02-01', 6, '2017-01-20', 0, 0, 9, NULL),
('197405152003121006', 'IRISU\'UD, SAP', 'Bangkalan', '1974-05-15', 'L', 15, '2015-10-01', '2003-12-01', 14, '1970-01-01', 6, 0, 11, NULL),
('197512032009031001', 'DIDIK HARTONO', 'Bangkalan', '1975-12-03', 'L', 9, '2015-10-01', '2009-03-01', 0, NULL, 1, 15, 30, NULL),
('197602022006041018', 'ANANG AFANDI, S.Kom', 'Bangkalan', '1976-02-02', 'L', 16, '2016-04-01', '2006-04-01', 15, '2017-01-20', 2, 8, 12, NULL),
('197605312006041010', 'UTAMA INDRAYANA, S, Sos', 'Surabaya', '1976-05-31', 'L', 16, '2018-04-01', '2006-04-01', 12, '1970-01-01', 2, 7, 14, NULL),
('197606162001122004', 'TULSI HAYATI, SE., MM', 'Bangkalan', '1976-06-16', 'P', 17, '2018-04-01', '2001-12-01', 13, '2017-01-20', 5, 0, 11, NULL),
('197610062008011008', 'HERIYANTO, SE', 'Bangkalan', '1976-10-06', 'L', 14, '2017-04-01', '2008-01-01', 0, NULL, 1, 15, 23, NULL),
('197610222009031001', 'RACHMAT SYAFIUDIN A, SE', 'Bangkalan', '1976-10-22', 'L', 15, '2017-04-01', '2009-03-01', 14, '2017-01-20', 4, 10, 12, NULL),
('197703182003121007', 'A. IMRON ROSYADI AL KHOIRI, SE., MM', 'Bangkalan', '1977-03-18', 'L', 16, '2017-10-01', '2003-12-01', 15, '2017-01-20', 1, 9, 13, NULL),
('197705272010011003', 'BACHTIAR ARIEF BUDIMAN, SH., MM', 'Bangkalan', '1977-05-27', 'L', 15, '2018-04-01', '2010-01-01', 0, NULL, 2, 8, 18, NULL),
('197711072010012002', 'NURUL HAYATI, SH., MM', 'Surabaya', '1977-11-07', 'P', 15, '2018-04-01', '2010-01-01', 0, NULL, 5, 11, 14, NULL),
('197806162014071001', 'MOHAMMAD MAKSYUM', 'Bangkalan', '1978-06-16', 'L', 8, '2016-04-01', '2014-07-01', 0, NULL, 1, 12, 32, NULL),
('197807292009032002', 'DIYANA YULIA ARIFIN, SE', 'Bangkalan', '1978-07-29', 'P', 16, '2018-04-01', '2009-03-01', 0, NULL, 1, 12, 15, NULL),
('197901161998102001', 'AINUN JARIYAH. S.STP.,M.Si', 'Bangkalan', '1979-01-16', 'P', 17, '2015-10-01', '1998-10-01', 13, '2017-01-20', 4, 0, 11, NULL),
('197910182010011001', 'ERFANDHI DWI WINDIARTO', 'Bangkalan', '1979-10-18', 'L', 10, '2018-04-01', '2010-01-01', 0, NULL, 1, 9, 29, NULL),
('197910192009032001', 'WAHYU HERLINA, SE., M.Si', 'Bangkalan', '1979-10-19', 'P', 14, '2013-04-01', '2009-03-01', 0, NULL, 6, 2, 22, NULL),
('198005212010011004', 'DODY SANTOSA YAHYA', 'Jember', '1980-05-21', 'L', 10, '2018-04-01', '2010-01-01', 0, NULL, 6, 2, 14, NULL),
('198011202009032001', 'RATNA ARIYANI', 'Bangkalan', '1980-11-20', 'P', 10, '2017-04-01', '2009-03-01', 0, NULL, 6, 6, 22, NULL),
('198103172008011008', 'ANDRY RIAWAN, SE', 'Bangkalan', '1981-03-17', 'L', 14, '2015-04-01', '2008-01-01', 0, NULL, 6, 4, 21, NULL),
('198108312006041010', 'AGUS SUHARYONO, SAP', 'Sumenep', '1981-08-31', 'L', 15, '2018-04-01', '2006-04-01', 14, '2017-01-20', 5, 14, 12, NULL),
('198109122005012010', 'NENI TRIANA, S.Sos', 'Pamekasan', '1981-09-12', 'P', 15, '2018-04-01', '2005-01-01', 0, NULL, 4, 5, 19, NULL),
('198110222014072004', 'IKA NURHAYATI', 'Sumenep', '1981-10-22', 'P', 8, '2016-04-01', '2014-07-01', 0, NULL, 1, 9, 34, NULL),
('198201172014071001', 'IRWAN HAKSARA', 'Gresik', '1982-01-17', 'L', 8, '2016-04-01', '2014-07-01', 0, NULL, 4, 5, 33, NULL),
('198202052010011027', 'WAHID, SH', 'Bangkalan', '1982-02-05', 'L', 15, '2018-04-01', '2010-01-01', 14, '2017-01-20', 5, 13, 12, NULL),
('198205222010011003', 'MOH. JUMIANTO, SE', 'Bangkalan', '1982-05-22', 'L', 4, '2017-10-01', '2010-01-01', 0, NULL, 1, 9, 26, NULL),
('198303092009031002', 'FAIZAL AMIRULLAH, S.Kom', 'Bangkalan', '1983-03-09', 'L', 13, '2016-10-01', '2009-03-01', 0, NULL, 2, 3, 25, NULL),
('198303122009031001', 'MUHAMMAD ZAINAL ARIFIN, SE', 'Bangkalan', '1983-03-12', 'L', 13, '2016-10-01', '2009-03-01', NULL, NULL, 2, 8, 35, NULL),
('198304212010012032', 'TINI SURYANENGRUM, SE., MM', 'Bangkalan', '1983-04-21', 'P', 15, '2018-04-01', '2010-01-01', NULL, NULL, 1, 15, 13, NULL),
('198311272010012024', 'AULIYA IKA PRATIWI, SE', 'Bangkalan', '1983-11-27', 'P', 15, '2018-04-01', '2010-01-01', 14, '2017-01-20', 1, 12, 13, NULL),
('198408262010012002', 'SRI MARYATI, SE', 'Bangkalan', '1984-08-26', 'P', 13, '2017-10-01', '2010-01-01', 0, NULL, 6, 6, 19, NULL),
('198502012010012001', 'SYARIFAH', 'Bangkalan', '1985-02-01', 'P', 9, '2016-10-01', '2010-01-01', 0, NULL, 4, 5, 31, NULL),
('198512282011012008', 'DHESY MARIA SHINTYA ASNOEN, SE', 'Bangkalan', '1985-12-28', 'P', 14, '2016-10-01', '2011-01-01', 0, NULL, 1, 15, 23, NULL),
('198606032014071001', 'ADNAN RIYADI', 'Pamekasan', '1986-06-03', 'L', 6, '2016-04-01', '2014-07-01', 0, NULL, 6, 2, 19, NULL),
('198606062009032006', 'MIRA DIAN FITRIA, ST., MT', 'Surabaya', '1986-06-06', 'P', 15, '2017-04-01', '2009-03-01', 14, '2017-01-20', 5, 11, 12, NULL),
('198607182010011002', 'ARI SAPUTRA, SE', 'Bangkalan', '1986-07-18', 'L', 13, '2018-10-01', '2010-01-01', 0, NULL, 6, 6, 19, NULL),
('198703302011011002', 'NURUL HUDA, SE', 'Bangkalan', '1987-03-30', 'L', 14, '2015-10-01', '2011-01-01', 0, NULL, 2, 3, 19, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan`
--

CREATE TABLE `pendidikan` (
  `nip` char(18) NOT NULL,
  `nama_instansi_pendidikan` varchar(100) NOT NULL,
  `jenjang` varchar(5) NOT NULL,
  `jurusan` varchar(100) DEFAULT NULL,
  `tahun_lulus` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan`
--

INSERT INTO `pendidikan` (`nip`, `nama_instansi_pendidikan`, `jenjang`, `jurusan`, `tahun_lulus`) VALUES
('196008011983031020', 'UNIVERSITAS TRUNOJOYO', 'S1', 'Ilmu Hukum', 2003),
('196008231986031015', 'SMAN 1 BANGKALAN', 'SMA', 'Ilmu Pengetahuan Sosial', 1993),
('196101161981032008', 'SEKOLAH TINGGI KEUANGAN INDONESIA SURABAYA', 'S1', 'Akuntansi', 1983),
('196212311981121018', 'UNIVERSITAS \"WR. SUPRATMAN\" SURABAYA', 'S2', 'Magister Manajemen', 2004),
('196312011987021002', 'SMA PEMBANGUNAN PERSIAPAN PAMEKASAN', 'SMA', 'Ilmu Pengetahuan Sosial', 1983),
('196401161993032002', 'UNIVERSITAS DR. SOETOMO SURABAYA', 'S1', 'Perikanan', 1989),
('196405051988031013', 'SMAN 2 BANGKALAN', 'SMA', 'Ilmu Pengetahuan Sosial', 1984),
('196406152001121002', 'UNIVERSITAS DARUL \'ULUM JOMBANG', 'S1', 'Teknik Informatika', 2009),
('196509302001122002', 'AKADEMI ADMINISTRASI PEMBANGUNAN \"PGRI\" BANGKALAN', 'D3', 'Administrasi Negara', 1991),
('196510131986031008', 'UNIVERSITAS PANCA MARGA PROBOLINGGO', 'S1', 'Ilmu Administrasi Negara', 2005),
('196603121987031008', 'UNIVERSITAS PANCA MARGA PROBOLINGGO', 'S1', 'Ilmu Administrasi Negara', 2005),
('196612311988031044', 'SMA PGRI PAMEKASAN', 'SMA', 'Ilmu Pengetahuan Sosial', 1985),
('196709192010011001', 'AKADEMI ADMINISTRASI PEMBANGUNAN \"PGRI\" BANGKALAN', 'D3', 'Administrasi Negara', 1992),
('196712311992021003', 'UNIVERSITAS ISLAM MALANG', 'S1', 'Ilmu Administrasi Negara', 1990),
('196804281993032010', 'UNIVERSITAS BANGKALAN MADURA', 'S1', 'Hukum Keperdataan', 1992),
('196902132007011016', 'SMAN 2 BANGKALAN', 'SMA', 'Ilmu -Ilmu Fisik', 1988),
('197201082007011012', 'SMKN 1 BANGKALAN', 'SMK', 'Perkantoran', 1991),
('197210202001121003', 'UNIVERSITAS BANGKALAN MADURA', 'S1', 'Manajemen', 1998),
('197308182009032001', 'STIE YAPAN SURABAYA', 'S1', 'Manajemen Pemasaran', 2009),
('197405131993021001', 'UNIVERSITAS DR. SOETOMO SURABAYA', 'S2', 'Magister Manajemen', 2009),
('197405152003121006', 'SEKOLAH TINGGI ILMU ADMINISTRASI NEGARA', 'S1', 'Manajemen Pembangunan Daerah', 2010),
('197512032009031001', 'SMA PGRI 2 BANGKALAN', 'SMA', 'Ilmu-Ilmu Biologi', 1994),
('197602022006041018', 'STIKOM SURABAYA', 'S1', 'Manajemen Informatika', 2001),
('197605312006041010', 'UNIVERSITAS HANG TUAH SURABAYA', 'S1', 'Ilmu Administrasi Negara', 2001),
('197606162001122004', 'UNIVERSITAS PUTRA BANGSA', 'S2', 'Magister Manajemen', 2003),
('197610062008011008', 'STIE YAPAN SURABAYA', 'S1', 'Manajemen SDM', 2010),
('197610222009031001', 'UNIVERSITAS MUHAMMADIYAH MALANG', 'S1', 'Manajemen', 2002),
('197703182003121007', 'UNIVERSITAS PEMBANGUNAN NASIONAL \"VETERAN\" JAWA TIMUR', 'S1', 'Manajemen', 2004),
('197705272010011003', 'UNIVERSITAS DR. SOETOMO', 'S2', 'Magister Manajemen', 2012),
('197711072010012002', 'UNIVERSITAS DR. SOETOMO', 'S2', 'Magister Manajemen', 2012),
('197806162014071001', 'SMAN 2 BANGKALAN', 'SMA', 'Ilmu Pengetahuan Alam', 1997),
('197807292009032002', 'UNIVERSITAS MERDEKA MALANG', 'S1', 'Akuntansi', 2001),
('197901161998102001', 'UNIVERSITAS WIJAYA PUTRA', 'S2', 'Magister Administrasi Publik', 2005),
('197910182010011001', 'SMAN 1 BANGKALAN', 'SMA', 'Ilmu Pengetahuan Alam', 1998),
('197910192009032001', 'UNIVERSITAS DARUL \'ULUM JOMBANG', 'S2', 'Ilmu Ekonomi', 2012),
('198005212010011004', 'SMAN 1 CLURING', 'SMA', 'Ilmu Pengetahuan Sosial', 1999),
('198011202009032001', 'SMKN 1 BANGKALAN', 'SMK', 'Sekretaris', 2000),
('198103172008011008', 'SMAN 2 BANGKALAN', 'SMA', 'Ilmu Pengetahuan Sosial', 1999),
('198108312006041010', 'SEKOLAH TINGGI ILMU ADMINISTRASI NEGARA', 'S1', 'Manajemen Pembangunan Daerah', 2010),
('198109122005012010', 'SEKOLAH TINGGI ILMU SOSIAL DAN ILMU POLITIK \"WASKITA DHARMA\" MALANG', 'S1', 'Ilmu Administrasi Negara', 2007),
('198110222014072004', 'SMAN 1 SUMENEP', 'SMA', 'Ilmu Pengetahuan Alam', 2000),
('198201172014071001', 'SMU DHARMA WANITA SURABAYA', 'SMA', 'Ilmu Pengetahuan Sosial', 2000),
('198202052010011027', 'UNIVERSITAS TRUNOJOYO', 'S1', 'Ilmu Hukum', 2008),
('198205222010011003', 'STIE YAPAN SURABAYA', 'S1', 'Manajemen SDM', 2011),
('198303092009031002', 'UNIVERSITAS 17 AGUSTUS 1945 SURABAYA', 'S1', 'Teknik Informatika', 2010),
('198303122009031001', 'UNIVERSITAS NAROTAMA SURABAYA', 'S1', 'Manajemen', 2008),
('198304212010012032', 'UNIVERSITAS DR. SOETOMO', 'S2', 'Magister Manajemen', 2012),
('198311272010012024', 'UNIVERSITAS AIRLANGGA', 'S1', 'Manajemen', 2008),
('198408262010012002', 'STIE YAPAN SURABAYA', 'S1', 'Manajemen Pemasaran', 2009),
('198502012010012001', 'PKBM QURNIA LONGKEK', 'PAKET', 'Ilmu Pengetahuan Sosial', 2008),
('198512282011012008', 'UNIVERSITAS TRUNOJOYO', 'S1', 'Manajemen', 2008),
('198606032014071001', 'SLTPN 1 PADEMAWU', 'SMP', '', 2002),
('198606062009032006', 'INSTITUT TEKNOLOGI SEPULUH NOPEMBER', 'S2', 'Arsitektur - Manajemen Pembangunan Kota', 2013),
('198607182010011002', 'UNIVERSITAS KARTINI SURABAYA', 'S1', 'Manajemen', 2011),
('198703302011011002', 'UNIVERSITAS TEKNOLOGI SURABAYA', 'S1', 'Manajemen', 2009);

-- --------------------------------------------------------

--
-- Table structure for table `sub_bidang`
--

CREATE TABLE `sub_bidang` (
  `id_sub_bidang` int(11) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `nama_sub_bidang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_bidang`
--

INSERT INTO `sub_bidang` (`id_sub_bidang`, `id_bidang`, `nama_sub_bidang`) VALUES
(1, 4, 'Perkawinan dan Perceraian'),
(2, 6, 'Identitas Penduduk'),
(3, 2, 'Pengolahan dan Penyajian Data Kependudukan'),
(4, 6, 'Pendataan Penduduk'),
(5, 4, 'Kelahiran'),
(6, 6, 'Pindah Datang Penduduk'),
(7, 2, 'Tata Kelola dan SDM Tekno, Info dan Komunikasi'),
(8, 2, 'Sistem Informasi dan Administrasi Kependudukan'),
(9, 1, 'Umum dan Kepegawaian'),
(10, 4, 'Perubahan Status Anak, Pewarganegaraan dan Kematian'),
(11, 5, 'Pemanfaatan Data dan Dokumen Kependudukan'),
(12, 1, 'Perencanaan dan Evaluasi'),
(13, 5, 'Kerjasama'),
(14, 5, 'Inovasi Pelayanan'),
(15, 1, 'Keuangan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bidang`
--
ALTER TABLE `bidang`
  ADD PRIMARY KEY (`id_bidang`);

--
-- Indexes for table `diklat`
--
ALTER TABLE `diklat`
  ADD PRIMARY KEY (`id_diklat`);

--
-- Indexes for table `eselon`
--
ALTER TABLE `eselon`
  ADD PRIMARY KEY (`id_eselon`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `pangkat`
--
ALTER TABLE `pangkat`
  ADD PRIMARY KEY (`id_pangkat`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `sub_bidang`
--
ALTER TABLE `sub_bidang`
  ADD PRIMARY KEY (`id_sub_bidang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bidang`
--
ALTER TABLE `bidang`
  MODIFY `id_bidang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `diklat`
--
ALTER TABLE `diklat`
  MODIFY `id_diklat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `eselon`
--
ALTER TABLE `eselon`
  MODIFY `id_eselon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `pangkat`
--
ALTER TABLE `pangkat`
  MODIFY `id_pangkat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `sub_bidang`
--
ALTER TABLE `sub_bidang`
  MODIFY `id_sub_bidang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
